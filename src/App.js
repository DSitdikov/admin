import 'antd/dist/antd.css';

import React, { Component } from 'react';
import './App.css';
import { NewsPage } from './pages/news';
import { HomePage } from './pages/home';
import { Switch, Route, withRouter } from 'react-router-dom';
import { Layout, Menu, Icon, Avatar } from 'antd';
import { DemandsPage } from './pages/demands';
import { Typography } from 'antd';
import { DemandStatus } from './enums/status';

const { Title } = Typography;

const { Header, Sider, Content } = Layout;

const SCREENS = {
    HOME: {
        title: 'Главная',
        route: '/',
        icon: 'home'
    },
    NEWS: {
        title: 'Новости',
        route: '/news',
        icon: 'global'
    },
    DEMANDS: {
        title: 'Заявки',
        route: '/demands',
        icon: 'file-text'
    },
    COMPLETE_DEMANDS: {
        title: 'Завершённые заявки',
        route: `/demands/${DemandStatus.COMPLETE}`,
        icon: 'check-circle'
    },
    UNPROCESSED_DEMANDS: {
        title: 'Необработанные заявки',
        route: `/demands/${DemandStatus.UNPROCESSED}`,
        icon: 'close-circle'
    },
    PROCESSED_DEMANDS: {
        title: 'Обработанные заявки',
        route: `/demands/${DemandStatus.PROCESSED}`,
        icon: 'clock-circle'
    },
    COMPLAINTS: {
        title: 'Жалобы',
        route: '/complaints',
        icon: 'home'
    }
};

class App extends Component {

    constructor(props) {
        super(props);

        const selectedPage = this.getSelectedPage() || {};
        this.state = {
            collapsed: false,
            title: selectedPage.title
        };
    }

    toggle = () => {
        this.setState(prevState => ({ collapsed: !prevState.collapsed }));
    };

    getSelectedPage = () => Object.values(SCREENS)
        .find(({ route }) => this.props.location.pathname === route);

    render() {
        const { title } = this.state;
        const { route } = this.getSelectedPage();
        const defaultSelectedKeys = [route];

        return (
            <Layout>
                <Sider width={280} trigger={null} collapsible collapsed={this.state.collapsed}>
                    <div className='logo'/>
                    <Menu theme='dark' mode='inline' defaultSelectedKeys={defaultSelectedKeys}>
                        {this.getMenuItem(SCREENS.HOME)}
                        <Menu.SubMenu
                            key='sub3'
                            title={<span><Icon type={SCREENS.DEMANDS.icon}/><span>{SCREENS.DEMANDS.title}</span></span>}
                        >
                            {this.getMenuItem(SCREENS.UNPROCESSED_DEMANDS)}
                            {this.getMenuItem(SCREENS.PROCESSED_DEMANDS)}
                            {this.getMenuItem(SCREENS.COMPLETE_DEMANDS)}
                        </Menu.SubMenu>
                        {this.getMenuItem(SCREENS.NEWS)}
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff' }}>
                        <Icon
                            className='trigger'
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />
                        <span className='divider'/>
                        <Title level={4}>{title}</Title>
                        <div className='profile'>
                            <Title level={4} className='name'>Ирина Леонидовна</Title>
                            <Avatar size={48} icon="user" />
                        </div>
                    </Header>
                    <Content
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            background: '#fff',
                            minHeight: 280,
                        }}
                    >
                        <Switch>
                            <Route path={SCREENS.HOME.route} component={HomePage} exact/>
                            <Route path={SCREENS.NEWS.route} component={NewsPage}/>
                            <Route
                                path={SCREENS.PROCESSED_DEMANDS.route}
                                component={DemandsPage}
                            />
                            <Route
                                path={SCREENS.UNPROCESSED_DEMANDS.route}
                                component={DemandsPage}
                            />
                            <Route
                                path={SCREENS.COMPLETE_DEMANDS.route}
                                component={DemandsPage}
                            />
                        </Switch>
                    </Content>
                </Layout>
            </Layout>
        );
    }

    getMenuItem = ({ title, route, icon }) => (
        <Menu.Item key={route} onClick={() => this.onMenuItemSelected({ title, route })}>
            <Icon type={icon}/>
            <span>{title}</span>
        </Menu.Item>
    );

    onMenuItemSelected = ({ title, route }) => {
        this.setState({ title });
        this.props.history.push(route);
    };
}

export default withRouter(App);
