import { env } from '../env/env';

export const NewsService = {
    publish: ({ message, title }) => {
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                description: message,
                img: 'https://picsum.photos/200/300',
                title
            })
        };
        return fetch(new Request(`${env.api}/news`, options));
    }
};
