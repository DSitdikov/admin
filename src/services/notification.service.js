import { env } from '../env/env';

export class NotificationService {
    instance;

    clientId = (new Date()).getTime();

    webSocket = new WebSocket(env.socketApi);
    observers = new Map();

    constructor() {
        this.webSocket.onopen = () => console.log('connection successfully established');
        this.webSocket.onmessage = this.onMessageReceived.bind(this);
    }

    emit(type, message) {
        const notification = {
            clientId: this.clientId,
            type,
            message
        };

        console.log('sending notification:', notification);

        this.webSocket.send(JSON.stringify(notification));
    }

    on(type, handler) {
        let handlers = this.observers.get(type);
        if (!handlers) {
            handlers = [];
            this.observers.set(type, handlers);
        }

        handlers.push(handler);
    }

    onMessageReceived(event) {
        const notification = JSON.parse(event.data);

        if (notification.clientId === this.clientId) {
            return;
        }

        console.log('notification received:', notification);

        const handlers = this.observers.get(notification.type) || [];
        handlers.forEach(handler => handler(notification.message));
    }

    static getInstance() {
        if (!NotificationService.instance) {
            NotificationService.instance = new NotificationService();
        }

        return NotificationService.instance;
    }
}
