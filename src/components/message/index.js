import React from 'react';
import { Typography } from 'antd';
const {Title} = Typography;

export function Message({content}) {
    return (
        <Title>{content}</Title>
    )
}
