import { PureComponent } from 'react';
import React from 'react';
import { Input, Button, Typography, Col, Icon, notification } from 'antd';
import { NewsService } from '../../services/news.service';

const { TextArea } = Input;
const { Title } = Typography;

export class NewsPage extends PureComponent {
    state = { message: 'Уважаемые жильцы, ', title: '' };

    render() {
        const { message, title } = this.state;

        return (
            <React.Fragment>
                <Col span={16}>
                    <Title level={3}>Написать</Title>
                    <Input placeholder='Заголовок' value={title} onChange={this.onTitleValueChanged}/>
                    <br/>
                    <br/>
                    <TextArea rows={4} value={message} onChange={this.onMessageValueChanged}/>
                </Col>

                <Col span={16}>
                    <br/>
                    <Button type='primary' onClick={this.onPublishButtonClicked}>Разослать</Button>
                </Col>
            </React.Fragment>
        );
    }

    onPublishButtonClicked = async () => {
        try {
            const {title, message} = this.state;
            await NewsService.publish({title, message});
            this.setState({title: '', message: 'Уважаемые жильцы, '});
            this.showSuccessNotification();
        } catch (e) {
            this.showFailedNotification();
        }
    };

    onTitleValueChanged = event => this.setState({ title: event.target.value });

    onMessageValueChanged = event => this.setState({ message: event.target.value });

    showFailedNotification = () => {
        notification.open({
            message: 'Что-то пошло не так'
        });
    };

    showSuccessNotification = () => {
        notification.open({
            message: 'Новость успешно опубликована!',
            icon: <Icon type='check-circle' style={{ color: '#108ee9' }}/>,
        });
    };
}
