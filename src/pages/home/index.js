import { PureComponent } from 'react';
import React from 'react';
import { Typography } from 'antd';

const { Title, Paragraph } = Typography;

export class HomePage extends PureComponent {
    render() {
        return (
            <React.Fragment>
                <Title>Добро пожаловать!</Title>
                <Paragraph>
                    Для того, чтобы начать работу с необработанными заявками, перейдите, пожалуйста с соотвествующий
                    пункт меню.
                </Paragraph>
            </React.Fragment>
        );
    }
}
