import { PureComponent } from 'react';
import React from 'react';
import { DemandsItem } from './item';
import { DemandCategory } from '../../enums/demand-category';
import { Message } from '../../components/message';
import { DemandStatus } from '../../enums/status';
import {state} from '../../store';
import { Button, notification } from 'antd';
import { NotificationService } from '../../services/notification.service';

export class DemandsPage extends PureComponent {
    state = {
        demands: state.demands
    };

    service = new NotificationService();

    componentDidMount() {
        this.service.on('demand', ({demand}) => {
            this.setState(prevState => ({demands: [...prevState.demands, demand]}));
            this.openNotification(demand);
        });
    }

    render() {
        const status = this.props.location.pathname.replace('/demands/', '');
        const entities = this.state.demands
            .filter(item => item.status === status)
            .sort(this.compare);

        if (entities.length) {
            return (
                <div className='grid'>
                    {entities.map(demand => <DemandsItem {...demand} onProcessPress={this.changeStatus}/>)}
                </div>
            );
        }

        switch (status) {
            case DemandStatus.UNPROCESSED:
                return (
                    <div className='center'>
                        <Message content='Все заявки обработаны успешно!'/>
                    </div>
                );
            case DemandStatus.PROCESSED:
                return (
                    <div className='center'>
                        <Message content='Нет обработанных заявок!'/>
                    </div>
                );
            default:
                return (
                    <div className='center'>
                        <Message content='Заявки отсутствуют'/>
                    </div>
                )
        }
    }

    changeStatus = (demandId, status) => {
        this.setState(prevState => {
            const { demands } = prevState;
            const targetDemand = demands.find(item => item.id === demandId);
            const updatedDemand = { ...targetDemand, status };

            const updatedDemands = [
                ...demands.filter(demand => demand.id !== demandId),
                updatedDemand
            ];
            return { demands: updatedDemands };
        });
    };

    compare = (a, b) => {
        if (a.category === DemandCategory.EMERGENCY && a.date > b.date) {
            return 1;
        } else if (a.date < b.date) {
            return -1;
        }

        return 0;
    };

    openNotification = demand => {
        const key = `open${Date.now()}`;
        const btn = (
            <Button type="primary" size="small" onClick={() => this.onProcessNotification(key)}>
                Перейти к заявкам
            </Button>
        );

        notification.open({
            message: 'Новая заявка!',
            description:
                `${demand.content}`,
            btn,
            key,
            onClose: () => {},
        });
    };

    onProcessNotification = key => {
        notification.close(key);
        this.props.history.push('/demands/unprocessed');
    }
}
