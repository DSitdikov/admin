import { Card, Col, Button } from 'antd';
import React, { PureComponent } from 'react';
import { Tag, Modal, Input } from 'antd';
import moment from 'moment';
import { DemandStatus } from '../../../enums/status';
import { DemandCategory } from '../../../enums/demand-category';

const { TextArea } = Input;

const { confirm } = Modal;

export class DemandsItem extends PureComponent {
    initialResponse = {
        process: `Уважаемый ${this.props.author.name},`,
        confirm: `Уважаемый ${this.props.author.name}, благодарим, что воспользовались услугами сервиса!`
    };

    state = {
        response: this.initialResponse
    };

    render() {
        const { content, category, author } = this.props;

        const className = category === DemandCategory.EMERGENCY && 'critical';

        return (
            <Col>
                <Card className={className} title={this.getFormattedDate()} extra={this.getActions()}>
                    <p>{content}</p>
                    <p>{author.name}, <a href={`tel:${author.phone}`}>{author.phone}</a></p>

                    <div className='card-footer'>
                        <Tag color={this.getTagColor()}>{category}</Tag>
                    </div>
                </Card>
            </Col>
        );
    }

    getTagColor = () => this.props.category === DemandCategory.EMERGENCY ? 'magenta' : 'blue';

    getFormattedDate = () => moment(this.props.date).format('HH:mm, DD.MM.YYYY');

    getActions = () => {
        const processButton = <Button
            href='#'
            type='primary'
            onClick={this.showConfirmProcessing}
            block
        >Обработать</Button>;
        const completeButton = <Button
            href='#'
            type='primary'
            onClick={this.showConfirmComplete}
            block
        >Завершить</Button>;
        return (
            <Button.Group>
                {this.props.status === DemandStatus.UNPROCESSED && processButton}
                {this.props.status === DemandStatus.PROCESSED && completeButton}
            </Button.Group>
        );
    };

    showConfirmComplete = () => {
        const { onProcessPress, id } = this.props;
        const { revertToInitialResponse } = this;
        confirm({
            title: 'Вы уверены, что хотите подтвердить завершение заявки?',
            okText: 'Подтвердить',
            cancelText: 'Отмена',
            iconType: 'exclamation-circle',
            content: (
                <TextArea
                    defaultValue={this.state.response.confirm}
                    rows={4}
                    onChange={event => this.onResponseChanged(event.target.value, 'complete')}
                />
            ),
            onOk() {
                return new Promise(resolve => {
                    setTimeout(() => {
                        onProcessPress(id, DemandStatus.COMPLETE);
                        revertToInitialResponse();
                        resolve();
                    }, 1000);
                }).catch(() => console.log('Oops errors!'));
            },
            onCancel() {},
        });
    };

    showConfirmProcessing = () => {
        const { onProcessPress, id } = this.props;
        const { revertToInitialResponse } = this;
        confirm({
            title: 'Введите ответ:',
            okText: 'Подтвердить',
            cancelText: 'Отмена',
            iconType: 'exclamation-circle',
            content: (
                <TextArea
                    defaultValue={this.state.response.process}
                    rows={4}
                    onChange={event => this.onResponseChanged(event.target.value, 'process')}
                />
            ),
            onOk() {
                return new Promise(resolve => {
                    setTimeout(() => {
                        onProcessPress(id, DemandStatus.PROCESSED);
                        revertToInitialResponse();
                        resolve();
                    }, 1000);
                }).catch(() => console.log('Oops errors!'));
            },
            onCancel() {},
        });
    };

    revertToInitialResponse = () => this.setState({ response: this.initialResponse });

    onResponseChanged = (value, type) => {
        this.setState(prevState => ({
            response: {
                ...prevState.response,
                [type]: value
            }
        }));
    };
}
