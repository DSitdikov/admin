export const DemandStatus = {
  PROCESSED: 'processed',
  COMPLETE: 'complete',
  UNPROCESSED: 'unprocessed'
};
