import { DemandStatus } from './enums/status';
import { DemandCategory } from './enums/demand-category';

export const state = {
    demands: [
        {
            id: 1,
            date: new Date(),
            status: DemandStatus.PROCESSED,
            category: DemandCategory.EMERGENCY,
            content: 'У нас в доме перегорела лампочка на 5-ом этаже',
            issue: 'Прорвало трубу',
            critical: true,
            author: {
                name: 'Большедворов Г. А.',
                phone: '+79645678964'
            }
        },
        {
            id: 2,
            date: new Date('07.14.2019 13:00'),
            status: DemandStatus.UNPROCESSED,
            category: DemandCategory.NORMAL,
            content: 'Лампочка перегорела',
            issue: 'Дом',
            critical: true,
            author: {
                name: 'Биченков Д. М.',
                phone: '+79456678964'
            }
        },
        {
            id: 3,
            date: new Date('05.14.2019 13:00'),
            status: DemandStatus.COMPLETE,
            category: DemandCategory.EMERGENCY,
            content: 'Лампочка перегорела',
            issue: 'Дом',
            author: {
                name: 'Даниил Н. А.',
                phone: '+79456788964'
            }
        },
        {
            id: 4,
            date: new Date(),
            status: DemandStatus.UNPROCESSED,
            category: DemandCategory.EMERGENCY,
            content: 'Лампочка перегорела',
            issue: 'Дом',
            author: {
                name: 'Мишарин О. В.',
                phone: '+79405678968'
            }
        },
        {
            id: 5,
            date: new Date('10.14.2019 13:00'),
            status: DemandStatus.COMPLETE,
            category: DemandCategory.NORMAL,
            content: 'Лампочка перегорела',
            issue: 'Дом',
            author: {
                name: 'Агупов Ш. З.',
                phone: '+79996789648'
            }
        }
    ]
};
